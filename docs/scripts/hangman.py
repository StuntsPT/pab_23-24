import random

def letter_placer(letter, spaces, word):
    """
    Places letters (more than once) in the 'spaces' variable.
    Takes a letter as input (str)
    Takes spaces as input (list)
    Takes the word as input (str)
    Returns spaces with more filled letters (list)
    """
    for position in range(len(word)):
        if letter == word[position]:
            spaces[position] = letter

    return spaces

words_dict = (("comida", "abacate"),
              ("vestuario", "sapato"),
              ("clima", "chuva"))

my_word = random.choice(words_dict)
print("Bem vindo ao jogo da forca de PAB 2023-2024!")
print(f"A dica e: '{my_word[0]}'.")
tries = 6
word_length = len(my_word[1])
spaces = list("-" * word_length)
print(f"A palavra e '{''.join(spaces)}'.")
warnings = False

while tries > 0:
    print(f"Tem {tries} tentativas.")
    letter = input("Por favor escolha uma letra: ")
    if len(letter) > 1:
        if not warnings:
            print("APENAS UMA LETRA!")
            warnings = True
        else:
            tries -= 1
            print("EU DISSE APENAS UMA LETRA! Gastou uma tentativa.")


    if letter in my_word[1]:
        spaces = letter_placer(letter, spaces, my_word[1])
        print("".join(spaces))
        if not "-" in spaces:
            print("Vitoria")
            break
    else:
        print("A letra escolhida nao existe na palavra.")
        tries -= 1
else:
    print("Esgotou as tentativas. Adeus.")


