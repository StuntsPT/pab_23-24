### classes.cd[2] = "Sequence data types and operations"

#### Programação Aplicada à Bioinformática 2023-2024

![Logo EST](presentation_assets/logo-ESTB.png)

Francisco Pina Martins

<p><img style="vertical-align:middle" src="presentation_assets/mastodon-icon.svg" alt="Mastodon icon" width="40px", height="40px"> <a href="https://scholar.social/@FPinaMartins">@FPinaMartins@scholar.social</a></p>

---

### Summary

* &shy;<!-- .element: class="fragment" -->The concept of immutability
* &shy;<!-- .element: class="fragment" -->String operations
* &shy;<!-- .element: class="fragment" -->Lists Vs. tuples
* &shy;<!-- .element: class="fragment" -->Lists and tuples operations

---

### Mutable Vs. Immutable data types

* &shy;<!-- .element: class="fragment" -->Imagine objects in pyton like transparent boxes
  * &shy;<!-- .element: class="fragment" -->In *immutable* types you can see the inside, but cannot change it
    * &shy;<!-- .element: class="fragment" -->*Strings*, *tuples*, and *integers*
  * &shy;<!-- .element: class="fragment" -->In *mutable* types, the box is open and you can change its contents
    * &shy;<!-- .element: class="fragment" -->*Lists*, *sets*, and *dictionaries*
* &shy;<!-- .element: class="fragment" -->Nevertheless, in python you cannot change an object's *type*

&shy;<!-- .element: class="fragment" -->![glass boxes](cd02_assets/glass_boxes.jpg)

|||

### Mutability examples

<div class="fragment">

``` python
my_list = [1, 3, 3]  # Whoops!
my_list[1] = 2
print(my_list)

my_string = "133"  # I did it again.
my_string[1] = 2  # Got lost in the game (error)
```

</div>

---

### String operations

* &shy;<!-- .element: class="fragment" -->Python excels at string manipulation
  * &shy;<!-- .element: class="fragment" -->At least 46 *methods*
  * &shy;<!-- .element: class="fragment" -->**Never** modify the original string
    * &shy;<!-- .element: class="fragment" -->Return a new string
* &shy;<!-- .element: class="fragment" -->External libraries are also available
  * &shy;<!-- .element: class="fragment" -->`re` module
* &shy;<!-- .element: class="fragment" -->But there are many other ways to handle strings!

&shy;<!-- .element: class="fragment" -->![string manipulation](cd02_assets/string.png)

|||

### String operations examples (1)

<div class="fragment">

``` python
part_1 = "Bio"
part_2 = "Informatics"

full_word = part_1 + part_2  # String concatenation
print(full_word)

print(full_word.upper())  # UPPERCASE
print(full_word.lower())  # lowercase
print(full_word.title())  # Titlecase

print(full_word)  # Remains unchanged!
```

</div>

|||

### String operations examples (2)

<div class="fragment">

``` python
gene_name = "Cytochrome Oxidase Subunit"
gene_part = 1

full_gene = gene_name + " " + gene_part  # Err...
full_gene = gene_name + " " + str(gene_part)  # One way
print(full_gene)

print(gene_name, gene_part)  # A workaround

full_gene = f"{gene_name} {gene_part}"  # Meet the 'f-strings'. Introduced in python 3.6 (December 2016)
```

</div>

&shy;<!-- .element: class="fragment" -->[Here](https://realpython.com/python-f-strings/) is a great resource on 'f-strings'

|||

### String operations examples (3)

* &shy;<!-- .element: class="fragment" -->Not all string methods return strings:

<div class="fragment">

``` python
gene_name = "Cytochrome Oxidase Subunit"  # From above
gene_part = 1  # From above
full_gene = f"{gene_name} {gene_part}"  # From above

print(full_gene.startswith("C"))  # Returns Bool (True or False)
print(full_gene.split())  # Returns a list
```

</div>

---

### List operations

* &shy;<!-- .element: class="fragment" -->While strings are a sequence of characters, lists are a sequence of *anything*
  * &shy;<!-- .element: class="fragment" -->Elements are ordered
* &shy;<!-- .element: class="fragment" -->List elements can be of different types
  * &shy;<!-- .element: class="fragment" -->Elements can be repeated
* &shy;<!-- .element: class="fragment" -->Unlike strings, lists are *mutable*
  * &shy;<!-- .element: class="fragment" -->Elements can be added and removed at will!

&shy;<!-- .element: class="fragment" -->![little list](cd02_assets/list.png)

|||

### List operations examples (1)

<div class="fragment">

``` python
aligners = ["mafft", "clustal", "muscle"]
phylogenetic_inference = ["RAxML", "PAUP*", "IQTree"]

bioinformatics_software = aligners + phylogenetic_inference  # List concatenation
print(bioinformatics_software)

print(aligners * 2)  # List multiplication

print(", ".join(phylogenetic_inference))  # Create a string from a list (this is actually a string method)
print([aligners, phylogenetic_inference])  # Look, a list of lists!
```

</div>

&shy;<!-- .element: class="fragment" -->This is a lot of fun, but all these can be applied to tuples as well. What makes lists special?

|||

### List operations examples (2)

<div class="fragment">

``` python
aligners = ["mafft", "clustal", "muscle"]
phylogenetic_inference = ["RAxML", "PAUP*", "IQTree"]
bioinformatics_software = aligners + phylogenetic_inference  # All from above

print(aligners[1])  # "clustal"
aligners[1] = "clustal omega"
print(aligners)  # Modified element
aligners.append("BLAST")  # Adds an element
print(aligners)
aligners.remove("BLAST")  # Remove BLAST
print(aligners)
print(aligners.pop())  # Remove **and return** the last element of a list (any element, in fact)
print(aligners)  # Proof the element was removed
aligners.insert(1, "SAMTools")
print(aligners)
```

</div>

---

### Tuple operations

* &shy;<!-- .element: class="fragment" -->Tuples, like lists are also a sequence of *anything*
  * &shy;<!-- .element: class="fragment" -->Elements are ordered
* &shy;<!-- .element: class="fragment" -->Tuple elements can be of different types
  * &shy;<!-- .element: class="fragment" -->Elements can be repeated
* &shy;<!-- .element: class="fragment" -->Like strings, tuples are *immutable*
  * &shy;<!-- .element: class="fragment" -->Elements can't be added and removed

&shy;<!-- .element: class="fragment" -->![little tuple here](cd02_assets/tuple.png)

|||

### Tuple operations examples (1)

* &shy;<!-- .element: class="fragment" -->Tuples are very useful to store lists of values that are unexpected to change
  * &shy;<!-- .element: class="fragment" -->GPS coordinates
  * &shy;<!-- .element: class="fragment" -->Known traits

<div class="fragment">

``` python
t_coords = (38.652212191631506, -9.048529017302446)  # Theoretical
p11_coords = (38.652064989981646, -9.048954294799096)  # Practicals 11
p12_coords = (38.652129021627246, -9.048661467341551)  # Practicals 12

print(t_coords.index(-9.048529017302446))  # Returns the element's index value
my_coords = [t_coords, p11_coords, p12_coords]  # Although coords are fixed, the POI list may change!
```

</div>

* &shy;<!-- .element: class="fragment" -->Reasons for using tuples:
  * &shy;<!-- .element: class="fragment" -->Use less memory then lists
  * &shy;<!-- .element: class="fragment" -->Can't be modified by accident
  * &shy;<!-- .element: class="fragment" -->Can be used as dictionary keys

---

### Common manipulations to sequence data types

* &shy;<!-- .element: class="fragment" -->Indexing
* &shy;<!-- .element: class="fragment" -->Slicing
* &shy;<!-- .element: class="fragment" -->Finding

<div class="fragment">

``` python
my_string = "Bioinformatics"
my_list = ["RAxML", "PAUP*", "IQTree"]
my_tuple = (38.652212191631506, -9.048529017302446)

# Indexing
print(my_string[-1])  # Reverse index!

# Slicing
print(my_list[:1])  # Careful! This is **exclusive**
print(my_list[1:])  # Careful! This is **inclusive**
print(my_string[3:7])  # "info"

# Finding
print(my_tuple.index(-9.048529017302446))  # Raises an exception if value not found

# Other neat tricks
print(my_string[::-1])  # What about that?
print(my_string[:-1])
```

</div>

---

### References

* [Introducing Python pp.18](https://www.oreilly.com/library/view/introducing-python-2nd/9781492051374/) (Paywall)
* [Introducing Python (Chapter 3)](https://www.oreilly.com/library/view/introducing-python-2nd/9781492051374/) (Paywall)
* [W3 Schools string methods](https://www.w3schools.com/python/python_ref_string.asp)
* [W3 Schools list methods](https://www.w3schools.com/python/python_lists_methods.asp)
