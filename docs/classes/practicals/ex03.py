#!/usr/bin/env python3

# Ex01
dna_seq = "GTAGCTGATGCTAGCTGTGATTATTCGTACGATTTGTCGTAGTGTCGTATGCGTAGCTGATGCGTAT"
print(dna_seq)

dna_seq_length = len(dna_seq)
print(dna_seq_length)

adenines_in_seq = dna_seq.count("A")

base_counts = {"A": adenines_in_seq,
               "C": dna_seq.count("C"),
               "G": dna_seq.count("G"),
               "T": dna_seq.count("T")}

print(base_counts)

percent_gc = (base_counts["G"] + base_counts["C"]) / dna_seq_length

print(f"The DNA sequence GC content is ~ {round(percent_gc * 100, 1)}%.")

rna_seq = dna_seq.replace("T", "U")
print(rna_seq)


# Ex02
dna_fragments = dna_seq.split("GAT")
print(f"Digesting the DNA sequence with the mentioned restriction enzyme would result in the following: {dna_fragments}")
print(f"The DNA sequence was cut in {len(dna_fragments)} fragments. The last fragment is {len(dna_fragments[-1])} base pairs long.")

dna_fragments.append("TACGGCATT")

middle_dna_fragments = dna_fragments[1:5]

rebuilt_dna_plus = "GAT".join(dna_fragments)  # The 'GAT' motif had been digested and needs to be added back!
print(rebuilt_dna_plus)


# Ex03
primers = ("AGCTGATG", "TTATTCGT", "TAGTGTCG", "ATGCGTAT")  # My order may not be the same as yours but it should no take any difference to solve the exercise
print(primers)
print(primers[::-1])

# One day you will be doing this with a list comprehension!
indeces = []
indeces.append(dna_seq.find(primers[0]))
indeces.append(dna_seq.find(primers[1]))
indeces.append(dna_seq.find(primers[2]))
indeces.append(dna_seq.find(primers[3]))
# Here you can just look at what you have, and answer the question. However, for that extra mile, take a look at this:

# Full automation
# First get the position (index) of the primer closest to 5'
five_primer_index = indeces.index(min(indeces))  # I dare you to untwine this =-)

# Then get the position of the primer closest to 3'
three_primer_index = indeces.index(max(indeces))

print(f"The best primer pair would be {primers[five_primer_index]} and {primers[three_primer_index]}, since they produce the longest sequence.")

