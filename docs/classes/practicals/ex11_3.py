#!/usr/bin/env python3

def read_fasta_file(filename):
    sequences = []
    try:
        with open(filename, 'r') as file:
            for line in file:
                if line.startswith('>'):
                    # Skip header lines
                    continue
                sequence = line.strip()
                sequences.append(sequence)
                print("Length of sequence: {} is {}".format(sequence, len(sequence)))
    except FileNotFoundError:
        print("Error: File not found.")
    except Exception as e:
        print("Error: {}".format(e))
    finally:
        # Cleanup: Close the file
        file.close() if 'file' in locals() and file else None


# Allow the user to enter the filename
filename = input("Enter the filename (FASTA format): ")
read_fasta_file(filename)
