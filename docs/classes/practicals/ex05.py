#!/usr/bin/env python3

# 01

sp_list01 = ["Antaresia childreni", "Antaresia maculosa", "Antaresia perthensis", "Antaresia stimsoni", "Apodora papuana", "Aspidites melanocephalus", "Aspidites ramsayi", "Bothrochilus boa", "Leiopython albertisii", "Morelia viridis", "Morelia imbricata", "Python brongersmai"]
sp_list02 = ["Liasis fuscus", "Liasis mackloti", "Liasis olivaceus", "Malayopython reticulatus", "Malayopython timoriensis", "Morelia azurea", "Morelia bredli", "Morelia carinata", "Morelia spilota", "Morelia viridis", "Python anchietae", "Python bivittatus", "Python curtus"]

timor_python = "Malayopython timoriensis"

if timor_python in sp_list01 or timor_python in sp_list02:
    presence = ""
else:
    presence = "not "
    
print(f"{timor_python} is {presence}present in the species list.")


olive_python = "Liasis olivaceus"

if olive_python in sp_list01:
    msg = f"{olive_python} is present in sp_list01."
elif olive_python in sp_list02:
    msg = f"{olive_python} is present in sp_list02."
else:
    msg = f"{olive_python} is not present in any list."

print(msg)

comm_species = set(sp_list01).intersection(set(sp_list02))

if len(comm_species) == 0:
    msg = "No common species found."
elif len(comm_species) == 1:
    msg = f"The species name '{list(comm_species).pop()}' is common to both lists."
else:
    msg = f"These are the species names common to both lists: {str(comm_species)[1:-1]}."

print(msg)

known_species = comm_species.pop()
# Alternative
# known_species = "Morelia viridis"

if known_species in sp_list01:
    if known_species in sp_list02:
        presence = "both lists"
    else:
        presence = "sp_list01"
elif known_species in sp_list02:
    presence = "sp_list02"
else:
    presence = "no list"

print(f"{known_species} is present in {presence}.")

my_sequences = ("ATGATGCATGCTAGTCTGATGCGCTGTTGA",
                "ATTCGATCGATTCGATCTCGATCGATAA",
                "CGATCAGCTAGCATCAGCATACGTACATA")
my_sequence = my_sequences[0]  # Just replace the index here to experiment with all 3 sequences

if my_sequence.startswith("ATG") and my_sequence.endswith(("TGA", "TAG", "TAA")) and len(my_sequence) % 3 == 0:
    msg = f"The sequence {my_sequence} is an ORF."
else:
    msg = f"The sequence {my_sequence} is not an ORF, because:\n"
    if not my_sequence.startswith("ATG"):
        msg += "It does not start with ATG.\n"
    if not my_sequence.endswith(("TGA", "TAG", "TAA")):
        msg += "It does not end with a stop codon.\n"
    if not len(my_sequence) % 3 == 0:
        msg += "It is not divisible by 3."

print(msg)

# Challange

# Example sequences:

sequence_with_f1_orf = "TTTATGAGTCGATCGAGCTAGCATCGATAGCTCGATCGATCATGATTT"  # Start codon on [3], stop codon on [-6]
sequence_with_fminus2_orf = "GTTGTATCGATCGATCTACGGTCAGTATTAG"  # Start codon on [-5], stop codon on [2]

my_seq = sequence_with_fminus2_orf  # Change here to test other sequences

start_codon_index = my_seq.find("ATG")
rev_start_codon_index = my_seq[::-1].find("ATG")
if start_codon_index != -1:
    seq_from_ATG = my_seq[start_codon_index:]
    print(f"A start codon was found on frame {(start_codon_index % 3) + 1}. The sequence from there is '{seq_from_ATG}'.")
elif rev_start_codon_index != -1:
    seq_from_ATG = my_seq[::-1][rev_start_codon_index:]
    print(f"A start codon was found on frame {(start_codon_index % 3) * -1}. The sequence from there is '{seq_from_ATG}'.")
else:
    print("No start codons found.")



