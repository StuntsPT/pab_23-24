# Exercises 07

## File I/O

In this session we will be working with everything from the previous classes, plus file I/O.


### 01 - Samples and coordinates

Consider the following [file](assets/ex07_coords.txt). It contains sampling sites and their respective coordinates from [this paper](https://doi.org/10.1111/gcb.14497). 

1. Download the file to your machine, open it with a text editor of your choice, and inspect it's contents.
2. Write code that prints each line in the file.
3. Write code that stores the file contents in a dictionary. The sample name should be the key, and the coordinates the value.
4. **WARNING: Tough one ahead.** Notice that relative to the paper, samples from the location "Var" and "Tun" are missing. Find the coordinates from the missing sampling sites in the paper, add them to your dictionary, and write a new file containing both the samples from `ex07_coords.txt` and the new entries.


# *Warning to all those who pass beyond this point:* Easy mode is **over**. Either study **hard**, or be prepared for some pain.


### 02 - Basic data filtering

Consider the following [file](assets/ex07_envfile.txt). It contains sampling site names, and the value for each of 14 bioclimatic environment variables from the same paper as in the previous exercise. Unfortunately the sample names are unsorted, and the data from "Bio3" is presented in ºF instead of ºC.

1. Download the file to your machine, open it with a text editor of your choice, and inspect it's contents.
2. Write code that converts 72ºF to ºC. You can find a conversion rule [here](https://www.metric-conversions.org/temperature/fahrenheit-to-celsius.htm).
3. Write a new file that is similar to `ex07_envfile.txt`, but with "Bio3" converted to ºC. *Hint:* Use the `round()` function to make your output look pretty!
4. Write a new file that is similar to the one produced in the previous exercise, but also contains each sampling site's geographical coordinates.


### 03 - VCF parsing

<script language="javascript"> 
function toggle() {
    var ele = document.getElementById("toggleText");
    var text = document.getElementById("displayText");
    if(ele.style.display == "block") {
            ele.style.display = "none";
        text.innerHTML = "I'm ready for this";
    }
    else {
        ele.style.display = "block";
        text.innerHTML = "AAAAHHHHH. I can't watch this anymore. Hide it!";
    }
} 
</script>

This problem was also taken from the same paper as the previous two, but here we take a deeper dive into bioinformatics, so take a deep breath. This is another "real world problem". You may want to come back to this one after becoming a more experienced python coder. *This is especially true for the second problem*. You have been warned.

<a id="displayText" href="javascript:toggle();">I'm not scared, you are scared!</a>
<div id="toggleText" style="display: none" markdown="1">

Consider the following [VCF file](assets/ex07_genotypes.vcf). You can read about VCF format [here](https://samtools.github.io/hts-specs/VCFv4.3.pdf). Skim through the documentation (and find out how missing data is represented), and then take a look at the file using a text editor.

1. Write a small program that reads the VCF file, and reports how much missing data there is **per SNP**. You can output to STDOUT something as simple as the SNP identifier followed by the amount of missing data in %. (Which means one line per SNP)
2. Write a small program that reads the VCF file, and reports how much missing data there is **per sample**. You can output to STDOUT something as simple as the sample identifier followed by the amount of missing data in %. (Which means one line per sample)

*Hints:*

* As with all previous exercises, the `.split()` and `.join()` methods are extremely useful. Use them without moderation.
* The `.count()` method is critical here. Make sure you understand how it works.

</br>

</div>


### Extra - Make it pretty

1. Go back to all the code you have written so far, and make it **look** good:
  * Make sure there are spaces around operators;
  * Make sure there are no spaces between function names and ();
  * Make sure there are no spaces between () and their **contents**;
  * Make sure there are no spaces **before** a comma, but there is one space **after** a comma;
  * Make sure your variables have no *non-ascii* characters;
  * Are all of your variable names descriptive? Avoid variable names like `aa` or other meaningless gibberish. This is **hard** to do at first, but I'm here to make sure you start by adhering to these principles. Trust me, you will thank me later.
