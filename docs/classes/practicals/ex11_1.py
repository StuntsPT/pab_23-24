#!/usr/bin/env python3

def validate_dna_sequence(sequence):
    valid_nucleotides = set("ATCG")
    try:
        for nucleotide in sequence:
            if nucleotide.upper() not in valid_nucleotides:
                raise ValueError("Invalid nucleotide: {}".format(nucleotide))
    except ValueError as e:
        print("Error: {}".format(e))
        # Replace invalid nucleotide with 'N'
        sequence = ''.join(['N' if n.upper() not in valid_nucleotides else n for n in sequence])
    finally:
        return sequence


while True:
    input_sequence = input("Enter a DNA sequence: ")
    validated_sequence = validate_dna_sequence(input_sequence)
    if validated_sequence:
        print("Valid DNA sequence: {}".format(validated_sequence))
        break
