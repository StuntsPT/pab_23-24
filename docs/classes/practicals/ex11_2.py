#!/usr/bin/env python3

import logging
from datetime import datetime

# Set up logging configuration
logging.basicConfig(filename='sequence_log.txt', level=logging.INFO)


def validate_dna_sequence_with_logging(sequence):
    valid_nucleotides = set("ATCG")
    try:
        for nucleotide in sequence:
            if nucleotide.upper() not in valid_nucleotides:
                raise ValueError("Invalid nucleotide: {}".format(nucleotide))
    except ValueError as e:
        print("Error: {}".format(e))
        # Replace invalid nucleotide with 'N'
        sequence = ''.join(['N' if n.upper() not in valid_nucleotides else n for n in sequence])
    finally:
        # Log the sequence and whether it was modified
        if sequence != input_sequence:
            logging.info("{} - Original sequence: {}".format(datetime.now(), input_sequence))
            logging.info("{} - Modified sequence: {}".format(datetime.now(), sequence))
        else:
            logging.info("{} - Valid sequence: {}".format(datetime.now(), sequence))
        return sequence


if __name__ == "__main__":
    while True:
        input_sequence = input("Enter a DNA sequence: ")
        validated_sequence = validate_dna_sequence_with_logging(input_sequence)
        if validated_sequence:
            print("Valid DNA sequence: {}".format(validated_sequence))
            break
