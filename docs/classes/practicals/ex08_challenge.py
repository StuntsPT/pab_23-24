#!/usr/bin/env python3
import sys

def vcf_missing_per_SNP(vcf_file):
    """
    Reads a VCF file and returns the percent missing data of each SNP
    Takes a VCF file as input
    Does not return anything
    """
    print("SNP_name\tMissing_porportion")
    with open(vcf_file, "r") as vcf:
        for lines in vcf:
            if lines.startswith("#CHROM"):
                inds = len(lines.split()) - 9
            if not lines.startswith("#"):
                lines = lines.split()
                name = lines[0]
                missing_count = ";".join(lines[9:]).count(".")
                missing_percent = missing_count / (inds * 2)
                print(f"{name}\t{round(missing_percent, 2)}")


def vcf_missing_per_sample(vcf_file):
    """
    Reads a VCF file and returns the percent missing data of each sample
    Takes a VCF file as input
    Does not return anything
    """
    print("Sample_name\tMissing_porportion")
    with open(vcf_file, "r") as vcf:
        for lines in vcf:
            if lines.startswith("#CHROM"):
                inds = {x: 0 for x in lines.strip().split()[9:]}  # Figure this one out. =-)
                linecount = 0
            if not lines.startswith("#"):
                linecount += 1
                lines = lines.split()
                snp_data = lines[9:]
                for i, ind in enumerate(inds.keys()):
                    inds[ind] += snp_data[i].count(".")

        for sample, missing in inds.items():
            print(f"{sample}\t{round(missing / (linecount * 2), 2)}")


if __name__ == "__main__":
    # Usage: python3 ex08_challange.py path/to/file.vcf <snp> OR <sample>
    if sys.argv[2] == "snp":
        vcf_missing_per_SNP(sys.argv[1])
    elif sys.argv[2] == "sample":
        vcf_missing_per_sample(sys.argv[1])
    else:
        print("Please choose wether you want to calculate SNP or sample missing data.")
