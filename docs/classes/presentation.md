### classes[0] = "Presentation"

#### Programação Aplicada à Bioinformática 2023-2024

![Logo EST](presentation_assets/logo-ESTB.png)

Francisco Pina Martins

<p><img style="vertical-align:middle" src="presentation_assets/mastodon-icon.svg" alt="Mastodon icon" width="40px", height="40px"> <a href="https://scholar.social/@FPinaMartins">@FPinaMartins@scholar.social</a></p>

---

## About me

* &shy;<!-- .element: class="fragment" -->Graduation in Marine Biology (2004)
* &shy;<!-- .element: class="fragment" -->MSc in "Evolutionary and Developmental Biology" (2006)
* &shy;<!-- .element: class="fragment" -->PhD in "Global Change Biology and Ecology" (2018)

---

## Informação prática

* &shy;<!-- .element: class="fragment" --> **Horário:**
  * &shy;<!-- .element: class="fragment" --> Teóricas: 2ª-Feira - 11:00 às 12:30 **Sala S1.01**
  * &shy;<!-- .element: class="fragment" --> Práticas LBI11: 3ª-Feira - 09:00 às 11:00 **Sala S1.10**
  * &shy;<!-- .element: class="fragment" --> Práticas LBI12: 4ª-Feira - 09:00 às 11:00 **Sala S1.10**
* &shy;<!-- .element: class="fragment" --> Perguntas? f.pina.martins©estbarreiro.ips.pt
* &shy;<!-- .element: class="fragment" --> [Moodle](https://moodle.ips.pt/2324/course/view.php?id=587)
* &shy;<!-- .element: class="fragment" --> Office hours: TBD

---

## Avaliação


* &shy;<!-- .element: class="fragment" --><font color="orange">Contínua</font> **OU** <font color="deeppink">exame 1ª e/ou 2ª época</font>
  * &shy;<!-- .element: class="fragment" --><font color="orange">Avaliação contínua</font>
    * &shy;<!-- .element: class="fragment" --><font color="forestgreen">20% - Contexto sala de aula</font>
    * &shy;<!-- .element: class="fragment" --><font color="forestgreen">20% - 1º trabalho</font>
    * &shy;<!-- .element: class="fragment" --><font color="forestgreen">20% - 2º trabalho</font>
    * &shy;<!-- .element: class="fragment" --><font color="cyan">40% - Teste</font>
        * &shy;<!-- .element: class="fragment" --><font color="cyan">Nota mínima de 40%</font>
* &shy;<!-- .element: class="fragment" -->Realizar 3/4 das componentes da <font color="orange">avaliação contínua</font> impede acesso a <font color="deeppink">exame de 1ª época</font>
  * &shy;<!-- .element: class="fragment" --><font color="deeppink">Avaliação por exame</font>
    * &shy;<!-- .element: class="fragment" --><font color="deeppink">100% - exame</font>

---

## Programming Vs. Coding

<div style="float:left; width:50%">

* &shy;<!-- .element: class="fragment" data-fragment-index="1"--><font color="#ffe624">Creating a program for a purpose, following a plan</font> 
* &shy;<!-- .element: class="fragment" data-fragment-index="2"--><font color="#ffe624">Uses analysis tools, modelling frameworks, testing strategies</font> 
* &shy;<!-- .element: class="fragment" data-fragment-index="3"--><font color="#ffe624">Requires a high expertise level</font> 
* &shy;<!-- .element: class="fragment" data-fragment-index="4"--><font color="#ffe624">Conceptualizes the guidelines and methods, focus on interoperability</font> 
* &shy;<!-- .element: class="fragment" data-fragment-index="5"--><font color="#ffe624">Delivers a fully working application</font> 

</div>

<div style="float:left; width:50%">

* &shy;<!-- .element: class="fragment" data-fragment-index="1"--><font color="#3c90e0">Writing code according to a specific goal/task</font> 
* &shy;<!-- .element: class="fragment" data-fragment-index="2"--><font color="#3c90e0">Uses a text editor/IDE and other helper tools</font> 
* &shy;<!-- .element: class="fragment" data-fragment-index="3"--><font color="#3c90e0">Minimal expertise required</font> 
* &shy;<!-- .element: class="fragment" data-fragment-index="4"--><font color="#3c90e0">Focus on implementation details</font> 
* &shy;<!-- .element: class="fragment" data-fragment-index="5"--><font color="#3c90e0">Delivers fractions of a program</font> 

</div>

---

## How will this work?

* &shy;<!-- .element: class="fragment" -->Theoretical classes divided in 2 sections
  * &shy;<!-- .element: class="fragment" --><font color="#ffe624">Programming</font>
  * &shy;<!-- .element: class="fragment" --><font color="#3c90e0">Coding</font>
* &shy;<!-- .element: class="fragment" -->Practical classes with exercises
  * &shy;<!-- .element: class="fragment" -->*Lots* of exercises

---

## Focus on python

* &shy;<!-- .element: class="fragment" -->There are *many, many* programming languages
* &shy;<!-- .element: class="fragment" -->For various reasons, we will focus on [python](https://python.org)
  * &shy;<!-- .element: class="fragment" -->Easy to read and write
  * &shy;<!-- .element: class="fragment" -->Large community
  * &shy;<!-- .element: class="fragment" -->Newbie friendly

&shy;<!-- .element: class="fragment" -->![Python logo](presentation_assets/python_logo.png)

---

### That is all for today!

---

### References

* [Programming Vs. coding](https://www.uopeople.edu/blog/coding-vs-programming/)
* [Python website](https://python.org)
* [Introducing Python](https://www.oreilly.com/library/view/introducing-python/9781449361167/)
