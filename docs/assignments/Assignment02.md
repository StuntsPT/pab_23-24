# Assignment 02


### Deadline:

January 26<sup>th</sup> 2024


### Format:

Work in pairs


### Delivery:

Zip file via E-mail. The python script **must** be named `FirstName_LastName_StudentNumber-FirstName_LastName_StudentNumber-fastq_parser.py`.


## Introduction

The learning of lower level python skills is now fading far in the rear view mirror, and you are growing as a coder. An important skill is to know how to use functions and classes, which you absolutely **must** use for this task.

As you already know, bioinformatics uses various file formats for different programs. One such format is [FASTQ](https://knowledge.illumina.com/software/general/software-general-reference_material-list/000002211). For this assignment, you will have to write a program that is able to read a file in this format, and perform some operations on it.


## Objectives

* Create a program that is able to read a FASTQ formatted file.
    * Store each read's bases and the respective quality values (as integers).
    * Calculate and store each read's average quality<span class="hidden">. This is very important: unless you are a human, use the geometric mean</span>.
    * Calculate and store each read's length.
* Furthermore, the program should be able to write the reads back into FASTQ format.
    * Discarding all reads with an average quality below a determined threshold.
    * Discarding all reads with a length below a determined threshold.
    * Write a report on how many reads were discarded due to low quality, and due to short length. Below is an example for a file named `my_test.fastq`.

``` YAML
FilteringReport:
  Filename: my_test.fastq
  TotalReads: 4000
  DiscardLowQual: 89
  DiscardLen: 11
  ReadsAfterFilter: 3900
```


## Rules

* The program must be written in Python 3.
* The program cannot be linear -> Most of your code (more than 90%) has to be inside functions and classes.
* All functions **must** take at least one parameter, but not all of them have to return something.
* You can import modules from Python's standard library (those included with Python), but not from third party packages (like BioPython).
* Input and output files must be provided to the program as command line arguments.
    * Input `fastq` file
    * Output `fastq` file
    * Output report file
* Quality and length thresholds must also be passed as command line arguments.
    * Minimum length threshold
    * Minimum average quality threshold
    * The program needs to work even if only one of these two arguments is passed
* Reads from the input file must be read into an "intermediate representation": an instance of a class called `Read`.
    * The FASTQ format has two variants - BASE33 and BASE64. For this assignment, you can always assume BASE33 (specifically Illumina 1.8+).
    * Assume there can never be repeated read names.
* Your script **must** be documented in a `README.md` file
    * Description
    * Usage


## Hints

* It is highly recommended that you use an argument parser like [argparse](https://docs.python.org/3/library/argparse.html). You can also use the simple `sys.argv`, but it will make your program a lot harder to write and use.
* This is a big problem. Don't forget to break it into smaller tasks so they are manageable.
* Implementing the length and average quality calculator as class methods can simplify the work for you (but feel free to do it anyway you deem fit!)
* Below is a table with the ASCII quality values in order (the first character is a quality value of 0 and the last is 50)

``` Text
!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRS
```

* However, understanding how BASE33 quality values are encoded might make your job easier! [This Wikipedia article](https://en.wikipedia.org/wiki/FASTQ_format#Encoding) is a good starting point.

## Optional "fluff"

Like before, completing this optional task will not improve your grade (nor will you be penalized for not trying it), but will certainly turn you into a better programmer. This is kind of hard, but feel free to try it anyway! Draw a box plot where each "box" represents the quality values from each position (of all reads). So if your longest read is 100bp long, your plot should have 100 boxes with a central tendency measurement and a dispersion measurement of the quality values. Have fun with this one!
