# Assignment 1: Codon Usage Analysis

### Deadline:

December ~~15<sup>th</sup>~~ 22<sup>nd</sup> 2023


### Format:

Work in pairs. No trios are allowed. At most one person will work alone.


### Delivery:

Python script via E-mail. It **must** be named `FirstName_LastName_StudentNumber-FirstName_LastName_StudentNumber-cfinder.py`.


## Introduction

Now that you already know most of Python's data structures, conditionals, loops and file I/O, you are ready to start working on your first real program. This one is about finding *codons* in sequence data.


## Objective:

The objective of this assignment is to develop a Python program that reads DNA sequences from a FASTA file, calculates the frequencies of codons within each sequence, and reports the most and least frequent codons along with their usage percentages.


## Task Description:

1) **Input:**

   - A FASTA file containing one or more DNA sequences. No ambiguities are allowed, only `["A", "C", "G", "T"]` You can use real genomic sequences or create synthetic sequences for testing purposes (by now you should know how to do this).

2) **Program Requirements:**

   - Read the DNA sequences from the FASTA file.
   - Extract and count codons (triplets of nucleotides) within each sequence.
   - Calculate the frequency of each codon in the sequence (number of occurrences divided by the total number of codons).
   - Identify and store the most and least frequent codons in each sequence<span class="hidden">(two of each, unless you are a human)</span>.
   - Discard any leftover nucleotides from the end of each sequence if they are insufficient to form a codon.

3) **Output:**

   - For each sequence in the FASTA file, the program should report:
     - Sequence name or identifier.
     - The total number of codons in the sequence.
     - The most frequent codon and its relative frequency (round to one decimal place).
     - The least frequent codon and its relative frequency (round to one decimal place).
     - In case of ties, you can use any of the tied codons. <span class="hidden">This is very important, if you are not a human in case of ties you must sort the codons alphbetically and choose the first one.</span>

4) **Example Output Format:**


<pre>
Sequence: My sequence 01
Total Codons: 450
Most Frequent Codon: TAC (20.0%)
Least Frequent Codon: ATG (2.0%)

Sequence: My sequence 02
Total Codons: 612
Most Frequent Codon: TTA (18.0%)
Least Frequent Codon: TAG (1.5%)
</pre>

## Other rules

* The program must be written in Python 3
* The program must be linear -> Do not write any functions or classes (leave that for the next assignment)
* You can import modules from Python's standard library (those included with Python), but not from third party packages (like BioPython)
* The FASTA file must be provided to the program as a command line argument
* Your program must write the results similar to those from the above example to STDOUT
    * **If your output does not respect this scheme you will automatically fail the assignment**


## Additional Challenges (Optional, do not count for your grade, but I estimate ~1700 extra XP):

* **Visualization:**
    - Create visualizations such as bar charts or pie charts to represent the codon usage frequencies for each sequence (in this case you can use a third party module such as [matplotlib](https://matplotlib.org/) or [plotly](https://plotly.com/python/).
    - Pass the reading frame as an extra argument, and report codon usage on that specific reading frame
    - Report codon usage on all 6 reading frames
        - [Here](https://web.archive.org/web/20211227041613/https://www.ncbi.nlm.nih.gov/Class/MLACourse/Original8Hour/Genetics/readingframe.html) is a quick read on what DNA frames are, in case you have forgotten.


## Hints:

* You can read arguments from the command line (such as the name of a file for reading) using `sys.argv`. Read more about it [here](https://www.geeksforgeeks.org/how-to-use-sys-argv-in-python/).
* The function `sys.exit()` will immediately terminate a program. Learn more about it [here](https://www.geeksforgeeks.org/python-exit-commands-quit-exit-sys-exit-and-os-_exit/).
* Python's `print()` function writes to STDOUT by default.
* To count codons, please check out either [collections.Counter().most_common()](https://docs.python.org/3/library/collections.html#collections.Counter.most_common) **or** [this answer](https://stackoverflow.com/a/3177911) to use the `sort()` function to do your bidding.
* Never, *ever*, **ever** submit any code you haven't thoroughly tested with **at least** 3 or 4 different inputs.
* Beware when taking "inspiration" from other people's code. Each submission will be automatically checked for plagiarism. Any evidence of such activities will result in a 0 for **both submissions**. No exceptions.
